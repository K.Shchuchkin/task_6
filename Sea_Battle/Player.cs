﻿using System;

namespace Sea_Battle
{
    class Player
    {
        public string name { get; protected set; }
        public Field PlayerField { get; protected set; }
        public Ship Battleship { get; protected set; }
        public Ship Cruiser { get; protected set; }
        public Ship Destroyer { get; protected set; }
        public Ship Submarine { get; protected set; }
        public Ship[] ships { get; protected set; }
        public Player()
        {
            PlayerField = new Field();
            Battleship = new Battleship();
            Cruiser = new Cruiser();
            Destroyer = new Destroyer();
            Submarine = new Submarine();
            ships = new Ship[] { Battleship, Cruiser, Destroyer, Submarine };
        }
        public virtual void Attack(Player EnemyPlayer) { }
        public virtual void PlaceShips() { }
    }
    class Player1 : Player
    {
        public Player1() : base()
        {
            Console.WriteLine("Enter your name: ");
            name = Console.ReadLine();
        }
        public override void Attack(Player EnemyPlayer)
        {
            int x, y;
            Console.WriteLine("Attack!\nx-coordinate: ");
            while (!int.TryParse(Console.ReadLine().ToLower(), out x) || (x >= PlayerField.field.GetLength(0) + 1 || x <= 0))
                Console.Write("Sorry, enter a valid x-coordinate (1 to 10): ");
            Console.WriteLine("y-coordinate: ");
            while (!int.TryParse(Console.ReadLine().ToLower(), out y) || (y >= PlayerField.field.GetLength(1) + 1 || y <= 0))
                Console.Write("Sorry, enter a valid y-coordinate (1 to 10): ");
            Field.Point p = new Field.Point(x, y);
            Console.WriteLine(EnemyPlayer.PlayerField.Attack(p, EnemyPlayer.ships));
        }
        public override void PlaceShips()
        {
            int x, y, angle;
            Field.Point p;
            foreach (var ship in ships)
            {
                while(true)
                {
                    Console.WriteLine($"Place {ship.Name}");
                    Console.WriteLine("x-coordinate (1 to 9)");
                    while (!int.TryParse(Console.ReadLine().ToLower(), out x) || (x >= PlayerField.field.GetLength(0) + 1 || x <= 0))
                        Console.Write("Sorry, enter a valid x-coordinate (1 to 10): ");
                    Console.WriteLine("y-coordinate (1 to 9)");
                    while (!int.TryParse(Console.ReadLine().ToLower(), out y) || (y >= PlayerField.field.GetLength(1) + 1 || y <= 0))
                        Console.Write("Sorry, enter a valid y-coordinate (1 to 10): ");
                    p = new Field.Point(x, y);
                    Console.WriteLine("rotation (0 - vertical, 1 - horizontal)");
                    while (!int.TryParse(Console.ReadLine().ToLower(), out angle) || (angle > 1 || angle < 0))
                        Console.Write("Sorry, enter a valid angle: ");
                    if (PlayerField.IsValid(ship, p, angle))
                        break;
                    else
                        Console.WriteLine("Sorry, you cannot place ship here");
                }
                PlayerField.PlaceShip(ship, p, angle);
            }
        }
    }
    class Computer : Player
    {
        private Random rand = new Random(DateTime.Now.Millisecond);
        public Computer() : base() { }
        public override void Attack(Player EnemyPlayer)
        {
            Field.Point p = new Field.Point(rand.Next(0, PlayerField.field.GetLength(0) - 1), rand.Next(0, PlayerField.field.GetLength(1) - 1));
            //EnemyPlayer.PlayerField.Attack(p, EnemyPlayer.ships);
        }
        public override void PlaceShips()
        {
            int x = rand.Next(0, PlayerField.field.GetLength(0) - 1);
            int y = rand.Next(0, PlayerField.field.GetLength(1) - 1);
            int angle = rand.Next(0, 2);
            Field.Point p = new Field.Point(x, y);
            foreach (var ship in ships)
            {
                while(!PlayerField.IsValid(ship, p, angle))
                {
                    x = rand.Next(0, PlayerField.field.GetLength(0) - 1);
                    y = rand.Next(0, PlayerField.field.GetLength(1) - 1);
                    angle = rand.Next(0, 2);
                    p = new Field.Point(x, y);
                }
                PlayerField.PlaceShip(ship, p, angle);
            }
        }
    }
}
