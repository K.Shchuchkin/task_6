﻿using System;
using System.Runtime.Serialization;

namespace Sea_Battle
{
    class Field
    {
        public class Point
        {
            public int Y { get; set; }
            public int X { get; set; }
            public Point(int x, int y)
            {
                X = x - 1;
                Y = y - 1;
            }
        }
        public char[,] field 
        {
            private set;
            get;
        }
        public Field()
        {
            field = new char[10, 10];
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = '.';
                }
            }
        }
        public void PlaceShip(Ship ship, Point p, int angle)
        {
            switch (angle)
            {
                case 0:
                    for (int i = 0; i < ship.LifeCapacity; i++)
                    {
                        field[i + p.X, p.Y] = ship.Name[0];
                    }
                    break;
                case 1:
                    for (int i = 0; i < ship.LifeCapacity; i++)
                    {
                        field[p.X, i + p.Y] = ship.Name[0];
                    }
                    break;
            }
        }
        public bool IsValid(Ship ship, Point p, int angle)
        {
            switch (angle)
            {
                case 0:
                    if (ship.LifeCapacity + p.X > field.GetLength(0))
                        return false;
                    for (int i = 0; i < ship.LifeCapacity; i++)
                    {
                        try
                        {
                            char empty = field[i + p.X, p.Y];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            return false;
                        }
                        if (field[i + p.X, p.Y] != '.') return false;
                    }
                    return true;
                case 1:
                    if (ship.LifeCapacity + p.Y > field.GetLength(1))
                        return false;
                    for (int i = 0; i < ship.LifeCapacity; i++)
                    {
                        try
                        {
                            char empty = field[p.X, p.Y + i];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            return false;
                        }
                        if (field[i + p.X, p.Y] != '.') return false;
                    }
                    return true;
            }
            return false;
        }
        public string Attack(Point p, Ship[] ship)
        {
            switch(field[p.X, p.Y])
            {
                case 'B':
                    field[p.X, p.Y] = 'x';
                    return ship[0].Attacked();
                case 'C':
                    field[p.X, p.Y] = 'x';
                    return ship[1].Attacked();
                case 'D':
                    field[p.X, p.Y] = 'x';
                    return ship[2].Attacked();
                case 'S':
                    field[p.X, p.Y] = 'x';
                    return ship[3].Attacked();
                default:
                    return "Miss";
            }
        }
    }
}
