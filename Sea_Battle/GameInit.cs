﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
namespace Sea_Battle
{
    class Program
    {
        static void Main(string[] args)
        {
            Player computer = new Computer();
            Player player1 = new Player1();
            computer.PlaceShips();
            //player1.PlaceShips();
            while(true)
            {
                Console.WriteLine("e - exit;\nenter - attack;\ns - serialize;\nd - deserialize");
                char.TryParse(Console.ReadLine(), out char exit);
                if (exit == 'e')
                    break;
                else if(exit == 's')
                {
                    List<Player> game = new List<Player>();
                    game.Add(player1);
                    game.Add(computer);
                    using (StreamWriter file = File.CreateText(Environment.CurrentDirectory + @"\save.txt"))
                    {
                        var jsonFormatter = JsonConvert.SerializeObject(game.ToArray(), Formatting.Indented);
                        file.Write(jsonFormatter);
                    }
                }

                player1.Attack(computer);
                //computer.Attack(player1.PlayerField);
            }
            Console.WriteLine("Thanks for playing.");
        }
    }
}
