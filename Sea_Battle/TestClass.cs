﻿using Xunit;

namespace Sea_Battle
{
    public class TestClass
    {
        [Fact]
        public void TestAttack()
        {
            Player computer = new Computer();
            string field = computer.PlayerField.Attack(
                new Field.Point(3, 3),
                computer.ships
                );
            string Hit = "Hit";
            Assert.Equal(field, Hit);
        }
    }
}
