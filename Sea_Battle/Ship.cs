﻿namespace Sea_Battle
{
    class Ship
    {
        public int LifeCapacity { get; protected set; }
        private string Status { get; set; }
        public string Name { get; private set; }
        public Ship(string name, int LC)
        {
            LifeCapacity = LC;
            Name = name;
            Status = "Good";
        }
        public string Attacked()
        {
            Status = "Wounded";
            if (--LifeCapacity == 0)
                return Destroyed();
            return "Ship is wounded";
        }
        private string Destroyed()
        {
            return Status = $"{Name} is destroyed";
        }
    }
    class Battleship : Ship
    {
        public Battleship() : base("Battleship", 4) { }
    }
    class Cruiser : Ship
    {
        public Cruiser() : base("Cruiser", 3) { }
    }
    class Destroyer : Ship
    {
        public Destroyer() : base("Destroyer", 2) { }
    }
    class Submarine : Ship
    {
        public Submarine() : base("Submarine", 1) { }
    }
}
